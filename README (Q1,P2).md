The dataset 'chrome_reviews.csv' has 7204 rows with 10 columns. The objective is to identify the positive text reviews (Text column) with their corresponding negative ratings (Star column). Negative ratings means ratings less than or equal to 2.

The text column had so many emojis so I used regex expression to remove the emojis in the text.

I used nltk library to import stopwords and for stemming which is used to remove stopwords and find the root word for every text in the text column.

After cleaning the text column, I used Sentiment Analyzer to rate the sentiment of the text. Filtered all the positive text into a new dataframe with their corresponding ratings.

I again filtered them with rating less than or equal to 2 into a new dataframe and deployed it into Flask server.
